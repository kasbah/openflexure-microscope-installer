#!/bin/bash

read -r -p "This will attempt to completely purge your system of all OpenFlexure Microscope software packages. Are you sure you want to continue? [y/N] " response < /dev/tty
if [[ $response =~ ^(yes|y|Y)$ ]]; then
    if [ $SUDO_USER ]; then user=$SUDO_USER; else user=`whoami`; fi
    userhome="$(eval echo ~$user)"
    echo "Working in $userhome"

    sudo systemctl stop ofmserver
    sudo systemctl disable ofmserver
    sudo rm -r "$userhome/.openflexure"
    sudo rm /etc/systemd/system/ofmserver.service
    rm -r "$userhome/openflexure-microscope-keyclient"
    rm -r "$userhome/openflexure-microscope-server"
fi


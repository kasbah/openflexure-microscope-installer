#!/bin/bash

: <<'DISCLAIMER'

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

This script is licensed under the terms of the MIT license.
Unless otherwise noted, code reproduced herein
was written for this script.

- Based on installer scripts written by the Pimoroni crew -

DISCLAIMER

# Script control variables
debugmode="no" # whether the script should use debug routines
overridechecks="no" # whether the script should override system checks for debug

# "Product" information
piplibname="openflexure-microscope" # the name of the lib in pip repo
piplibver="==1.0.0rc3" # the version rules of the lib in pip repo
gitlibname="openflexure-microscope-server"
giturl="https://gitlab.com/openflexure/openflexure-microscope-server.git"
productname="OpenFlexure Microscope" # the name of the product to install
scriptname="openflexure_microscope.sh" # the name of this script
getpool="https://gitlab.com/openflexure/openflexure-microscope-installer/raw/master" # TODO: we need to set this up
confdir=".openflexure" # Name of the config directory to be added to HOME

# Python support and requirements
pipdeplist=('gunicorn' 'https://www.piwheels.org/simple/numpy/numpy-1.16.1-cp35-cp35m-linux_armv7l.whl#sha256=421b0b69975b7382ba4d7e3084e4066ccae4ed8060e8238ec73555cf9d69e588') # list of dependencies to source from pypi
pipoverride="yes" # whether the script should give priority to pip repo over git
pip2support="no" # whether python2 is supported
pip3support="yes" # whether python3 is supported
virtualenvname="openflexure_microscope" # virtualenv directory name

# Extras
raspapurl="https://raw.githubusercontent.com/jtc42/raspap-webgui/master/installers/raspbian.sh"

# System requirements
spacereq=100 # minimum size required on root partition in MB
armhfonly="yes" # whether the script is allowed to run on other arch
armv6="yes" # whether armv6 processors are supported
armv7="yes" # whether armv7 processors are supported
armv8="yes" # whether armv8 processors are supported
raspbianonly="no" # whether the script is allowed to run on other OSes
osreleases=( "Raspbian" ) # list os-releases supported
oswarning=( "Debian" "Ubuntu") # list experimental os-releases

# Non-python requirements
coredeplist=( "libatlas-base-dev" "libjasper-dev" "libjpeg-dev" )

# Permission requirements
forcesudo="yes" # whether the script requires to be ran with root privileges

# Hardware requirements
gpioreq="yes" # whether low-level gpio access is required
pcamreq="yes" # whether picamera access is required

# Arguments
FORCE=false
DEVMODE=false
while getopts yd o; do
	case $o in
		(y) FORCE=true;;
		(d) DEVMODE=true;;
	esac
done

# Functions

check_network() {
	sudo ping -q -w 10 -c 1 1.1.1.1 | grep "received, 0" &> /dev/null && return 0 || return 1
}

apt_pkg_req() {
	APT_CHK=$(dpkg-query -W -f='${Status}\n' "$1" 2> /dev/null | grep "install ok installed")

	if [ "" == "$APT_CHK" ]; then
		echo "$1 is required"
		true
	else
		echo "$1 is already installed"
		false
	fi
}

apt_pkg_install() {
	echo "Installing $1..."
	sudo apt-get --yes install "$1" 1> /dev/null || { inform "Apt failed to install $1!\nFalling back on pypi..." && return 1; }
}

confirm_assume_no() {
	if [ "$FORCE" = true ]; then
		false
	else
		read -r -p "$1 [y/N] " response < /dev/tty
		if [[ $response =~ ^(yes|y|Y)$ ]]; then
			true
		else
			false
		fi
	fi
}

confirm() {
	if [ "$FORCE" = true ]; then
		true
	else
		read -r -p "$1 [Y/n] " response < /dev/tty
		if [[ $response =~ ^(no|n|N)$ ]]; then
			false
		else
			true
		fi
	fi
}

# Prompt defaulting to no
prompt_assume_no() {
		read -r -p "$1 [y/N] " response < /dev/tty
		if [[ $response =~ ^(yes|y|Y)$ ]]; then
			true
		else
			false
		fi
}

# Prompt defaulting to yes
prompt() {
		read -r -p "$1 [Y/n] " response < /dev/tty
		if [[ $response =~ ^(no|n|N)$ ]]; then
			false
		else
			true
		fi
}

success() {
	echo -e "$(tput setaf 2)$1$(tput sgr0)"
}

inform() {
	echo -e "$(tput setaf 6)$1$(tput sgr0)"
}

warning() {
	echo -e "$(tput setaf 1)$1$(tput sgr0)"
}

newline() {
	echo ""
}

progress() {
	count=0
	until [ $count -eq 7 ]; do
		echo -n "..." && sleep 1
		((count++))
	done;
	if ps -C $1 > /dev/null; then
		echo -en "\r\e[K" && progress $1
	fi
}

sudocheck() {
	if [ $(id -u) -ne 0 ]; then
		echo -e "Install must be run as root. Try 'sudo ./$scriptname'\n"
		exit 1
	fi
}

sysclean() {
	sudo apt-get clean && sudo apt-get autoclean
	sudo apt-get -y autoremove &> /dev/null
}

sysupdate() {
	if ! $UPDATE_DB; then
		echo "Updating apt indexes..." && progress apt-get &
		sudo apt-get update 1> /dev/null || { warning "Apt failed to update indexes!" && exit 1; }
		sleep 3 && UPDATE_DB=true
	fi
}

sysupgrade() {
	sudo apt-get upgrade
	sudo apt-get clean && sudo apt-get autoclean
	sudo apt-get -y autoremove &> /dev/null
}

sysreboot() {
	warning "Some changes made to your system require"
	warning "your computer to reboot to take effect."
	echo
	if prompt_assume_no "Would you like to reboot now?"; then
		sync && sudo reboot
	fi
}

arch_check() {
	IS_ARMHF=false
	IS_ARMv6=false

	if uname -m | grep -q "armv.l"; then
		IS_ARMHF=true
		if uname -m | grep -q "armv6l"; then
			IS_ARMv6=true
		fi
	fi
}

os_check() {
	IS_MACOSX=false
	IS_RASPBIAN=false
	IS_SUPPORTED=false
	IS_EXPERIMENTAL=false
	OS_NAME="Unknown"

	if uname -s | grep -q "Darwin"; then
		OS_NAME="Darwin" && IS_MACOSX=true
	elif cat /etc/os-release | grep -q "Kali"; then
		OS_NAME="Kali"
	elif [ -d ~/.kano-settings ] || [ -d ~/.kanoprofile ]; then
		OS_NAME="Kano"
	elif whoami | grep -q "linaro"; then
		OS_NAME="Linaro"
	elif [ -d ~/.config/ubuntu-mate ];then
		OS_NAME="Mate"
	elif [ -d ~/.pt-os-dashboard ] || [ -d ~/.pt-dashboard ] || [ -f ~/.pt-dashboard-config ]; then
		OS_NAME="PiTop"
	elif command -v emulationstation > /dev/null; then
		OS_NAME="RetroPie"
	elif cat /etc/os-release | grep -q "OSMC"; then
		OS_NAME="OSMC"
	elif cat /etc/os-release | grep -q "volumio"; then
		OS_NAME="Volumio"
	elif cat /etc/os-release | grep -q "Raspbian"; then
		OS_NAME="Raspbian" && IS_RASPBIAN=true
	elif cat /etc/os-release | grep -q "Debian"; then
		OS_NAME="Debian"
	elif cat /etc/os-release | grep -q "Ubuntu"; then
		OS_NAME="Ubuntu"
	fi

	if [[ " ${osreleases[@]} " =~ " ${OS_NAME} " ]]; then
		IS_SUPPORTED=true
	fi
	if [[ " ${oswarning[@]} " =~ " ${OS_NAME} " ]]; then
		IS_EXPERIMENTAL=true
	fi
}

raspbian_check() {
	IS_SUPPORTED=false
	IS_EXPERIMENTAL=false

	if [ -f /etc/os-release ]; then
		if cat /etc/os-release | grep -q "/sid"; then
			IS_SUPPORTED=false && IS_EXPERIMENTAL=true
		elif cat /etc/os-release | grep -q "stretch"; then
			IS_SUPPORTED=true && IS_EXPERIMENTAL=false
		elif cat /etc/os-release | grep -q "jessie"; then
			IS_SUPPORTED=true && IS_EXPERIMENTAL=false
		elif cat /etc/os-release | grep -q "wheezy"; then
			IS_SUPPORTED=true && IS_EXPERIMENTAL=false
		else
			IS_SUPPORTED=false && IS_EXPERIMENTAL=false
		fi
	fi
}

home_dir() {
	if [ $EUID -ne 0 ]; then
		if $IS_MACOSX; then
			USER_HOME=$(dscl . -read /Users/$USER NFSHomeDirectory | cut -d: -f2)
		else
			USER_HOME=$(getent passwd $USER | cut -d: -f6)
		fi
	else
		warning "Running as root, please log in as a regular user with sudo rights!"
		echo #&& exit 1
	fi
}

space_chk() {
	if command -v stat > /dev/null && ! $IS_MACOSX; then
		if [ $spacereq -gt $(($(stat -f -c "%a*%S" /)/10**6)) ];then
			echo
			warning  "There is not enough space left to proceed with  installation"
			echo #&& exit 1
		fi
	fi
}

pip_cmd_chk() {
	if command -v pip2 > /dev/null; then
		PIP2_BIN="pip2"
	elif command -v pip-2.7 > /dev/null; then
		PIP2_BIN="pip-2.7"
	elif command -v pip-2.6 > /dev/null; then
		PIP2_BIN="pip-2.6"
	else
		PIP2_BIN="pip"
	fi
	if command -v pip3 > /dev/null; then
		PIP3_BIN="pip3"
	elif command -v pip-3.3 > /dev/null; then
		PIP3_BIN="pip-3.3"
	elif command -v pip-3.2 > /dev/null; then
		PIP3_BIN="pip-3.2"
	fi
}

py_cmd_chk() {
	if command -v python2 > /dev/null; then
		PY2_BIN="python2"
	elif command -v pip-2.7 > /dev/null; then
		PY2_BIN="python2.7"
	elif command -v python2.6 > /dev/null; then
		PY2_BIN="python2.6"
	else
		PY2_BIN="python"
	fi
	if command -v python3 > /dev/null; then
		PY3_BIN="python3"
	elif command -v python3.3 > /dev/null; then
		PY3_BIN="python3.3"
	elif command -v python3.2 > /dev/null; then
		PY3_BIN="python3.2"
	fi
}

pip2_lib_req() {
	PIP2_CHK=$($PIP2_BIN list 2> /dev/null | grep -i "$1")

	if [ -z "$PIP2_CHK" ]; then
		true
	else
		false
	fi
}

pip3_lib_req() {
	PIP3_CHK=$($PIP3_BIN list 2> /dev/null | grep -i "$1")

	if [ -z "$PIP3_CHK" ]; then
		true
	else
		false
	fi
}


: <<'MAINSTART'

Perform all variables declarations as well as function definition
above this section for clarity, thanks!

MAINSTART

# intro message
echo -e "\nThis script will install everything needed to use\n$productname"
if [ "$FORCE" != true ]; then
	inform "\nAlways be careful when running scripts and commands copied"
	inform "from the internet. Ensure they are from a trusted source.\n"
	echo -e "If you want to see what this script does before running it,"
	echo -e "you should run: 'curl $getpool/$scriptname'\n"
fi

# checks and init

arch_check
os_check
space_chk
home_dir

if [ $debugmode != "no" ]; then
	echo "USER_HOME is $USER_HOME"
	echo "OS_NAME is $OS_NAME"
	echo "IS_SUPPORTED is $IS_SUPPORTED"
	echo "IS_EXPERIMENTAL is $IS_EXPERIMENTAL"
fi

if [ $overridechecks == "yes" ]; then
	if ! $IS_ARMHF; then
		warning "This hardware is not supported, sorry!"
		warning "Config files have been left untouched\n"
		exit 1
	fi

	if $IS_ARMv8 && [ $armv8 == "no" ]; then
		warning "Sorry, your CPU is not supported by this installer\n"
		exit 1
	elif $IS_ARMv7 && [ $armv7 == "no" ]; then
		warning "Sorry, your CPU is not supported by this installer\n"
		exit 1
	elif $IS_ARMv6 && [ $armv6 == "no" ]; then
		warning "Sorry, your CPU is not supported by this installer\n"
		exit 1
	fi

	if [ $raspbianonly == "yes" ] && ! $IS_RASPBIAN;then
		warning "This script is intended for Raspbian on a Raspberry Pi!\n"
		exit 1
	fi

	if $IS_RASPBIAN; then
		raspbian_check
		if ! $IS_SUPPORTED && ! $IS_EXPERIMENTAL; then
			warning "\n--- Warning ---\n"
			echo "The $productname installer"
			echo "does not work on this version of Raspbian."
			echo "Check https://github.com/$gitusername/$gitreponame"
			echo "for additional information and support" && echo
			exit 1
		fi
	fi

	if ! $IS_SUPPORTED && ! $IS_EXPERIMENTAL; then
		warning "Your operating system is not supported, sorry!\n"
		exit 1
	fi
fi

if $IS_EXPERIMENTAL; then
	warning "\nSupport for your operating system is experimental. Please visit"
	warning "forums.pimoroni.com if you experience issues with this product.\n"
fi

if [ $forcesudo == "yes" ]; then
	sudocheck
fi

newline
if confirm "Do you wish to continue?"; then

# Figure out who is running the script as sudo, build home directory
	if [ $SUDO_USER ]; then user=$SUDO_USER; else user=`whoami`; fi
	userhome="$(eval echo ~$user)"
	echo "Working in $userhome"

# basic environment preparation

	echo -e "\nChecking environment..."

	if [ "$FORCE" != '-y' ]; then
		if ! check_network; then
			warning "We can't connect to the Internet, check your network!" #&& exit 1
		fi
		sysupdate && newline
	fi

	# Basic packages
	if apt_pkg_req "apt-utils" &> /dev/null; then
		apt_pkg_install "apt-utils"
	fi
	if ! command -v curl > /dev/null; then
		apt_pkg_install "curl"
	fi
	if ! command -v wget > /dev/null; then
		apt_pkg_install "wget"
	fi

	# Check and install pip as required
	if [ "$pip3support" == "yes" ]; then
		if ! [ -f "$(which python3)" ]; then
			if confirm "Python 3 is not installed. Would like to install it?"; then
				progress apt-get &
				apt_pkg_install "python3-pip"
			else
				pip3support="na"
			fi
		elif apt_pkg_req "python3-pip" &> /dev/null; then
			progress apt-get &
			apt_pkg_install "python3-pip"
		fi
	fi
	if [ "$pip2support" == "yes" ]; then
		if ! [ -f "$(which python2)" ]; then
			if confirm "Python 2 is not installed. Would like to install it?"; then
				progress apt-get &
				apt_pkg_install "python-pip"
			else
				pip2support="na"
			fi
		elif apt_pkg_req "python-pip" &> /dev/null; then
			progress apt-get &
			apt_pkg_install "python-pip"
		fi
	fi
	# Find available versions of python and pip
	pip_cmd_chk
	py_cmd_chk


# Pick one python environment to work with
	if [ -f "$(which python3)" ] && [ $pip3support == "yes" ]; then
		PIP_BIN="$PIP3_BIN"
		PY_BIN="$PY3_BIN"
	elif [ -f "$(which python2)" ] && [ $pip2support == "yes" ]; then
		PIP_BIN="$PIP2_BIN"
		PY_BIN="$PY2_BIN"
	else
		echo "Somehow, no Python environment can be found. This shouldn't be able to happen. Exiting.\nSorry about that..."
		exit 1
	fi


# General OpenFlexure environment preparation

    # Enable SSH
    echo "Enabling SSH..."
    sudo raspi-config nonint do_ssh 0

	# Install apt requirement
	echo "Installing core dependencies..."
	for pkgdep in ${coredeplist[@]}; do
		if apt_pkg_req "$pkgdep"; then
			apt_pkg_install "$pkgdep"
		fi
	done

	# Create openflexure directory
	userconfdir="$userhome/$confdir"

	if [ ! -d $userconfdir ]; then
		newline
		echo -e "Creating config directory at $userconfdir"
		mkdir "$userconfdir"
	fi

	# Create virtualenv
	virtualenvdir="$userconfdir/virtualenv/$virtualenvname"
	newline
	echo -e "Creating virtual environment in $virtualenvdir"

	$PIP_BIN install "virtualenv"

	if [ -d $virtualenvdir ]; then
		echo "Virtualenv directory already exists. Skipping to activation."
	else
		$PY_BIN -m virtualenv "$virtualenvdir"
	fi
	echo "Activating virtualenv..."
	source "$virtualenvdir/bin/activate"

	# Set pip bin paths to the virtualenv
	PY_BIN="$virtualenvdir/bin/$PY_BIN"
	PIP_BIN="$virtualenvdir/bin/$PIP_BIN"
	echo "$(which $PY_BIN)"
	echo "$(which $PIP_BIN)"

# Raspberry Pi hardware setup

	# Check and enable picamera
	if [ $pcamreq == "yes" ]; then
		newline
		echo -e "Enabling picamera."
		sudo raspi-config nonint do_camera 0
		sudo raspi-config nonint do_memory_split 256
	fi

	# Check and install GPIO packages and Python modules
	if [ $gpioreq == "yes" ]; then
		newline
		echo -e "Checking for packages required for GPIO control..."
		if apt_pkg_req "raspi-gpio"; then
			apt_pkg_install "raspi-gpio"
		fi

		echo "Installing Python RPi.GPIO"
		sudo $PIP_BIN install RPi.GPIO && FAILED_PKG=false

		if ! $PIP_BIN list | grep "RPi.GPIO" &> /dev/null; then
			warning "Unable to install RPi.GPIO for python!" && FAILED_PKG=true
		else
			RPIGPIO="install ok installed"
		fi

		if [ "$RPIGPIO" == "install ok installed" ]; then
			if ! $FAILED_PKG; then
				echo -e "RPi.GPIO installed and up-to-date"
			fi
		fi
	fi

# openflexure environment installation

	# Install additional deps
	echo -e "Installing additional required Python packages."
	for pipdep in ${pipdeplist[@]}; do
		sudo $PIP_BIN install "$pipdep"
	done

	# Install main library
	newline
	echo -e "Installing $productname"

	if [ "$DEVMODE" = true ]; then
		# If a developer, use git and poetry
		if command -v poetry > /dev/null; then
			echo "Poetry already installed"
		else
			# Install poetry
			curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
			# Add poetry to path
			source $HOME/.poetry/env
		fi

		# Clone repo
		if [ ! -d $userhome/$gitlibname ]; then
			echo "Creating $userhome/$gitlibname"
			mkdir "$userhome/$gitlibname"
		else
			echo "$userhome/$gitlibname already exists and must be removed in order to git clone."
			if confirm "Remove existing directory and clone?"; then
				rm -r "$userhome/$gitlibname"
				mkdir "$userhome/$gitlibname"
			fi
		fi
		git clone "$giturl" "$userhome/$gitlibname"
		
		# Poetry install
		newline
		echo -e "Poetry-installing $piplibname. This may take a while!"
		cd $userhome/$gitlibname
	    chown -R $user:$user "$userhome/$gitlibname"
		poetry install
		cd $userhome
	else
		sudo $PIP_BIN install "$piplibname$piplibver"
	fi

	# Check library installed
	if ! $PIP_BIN list | grep "$piplibname" &> /dev/null; then
		echo # Separator
		warning "Unable to install $piplibname. Exiting." && FAILED_PKG=true
		exit 1
	fi

	# Install lens-shading fork of picamera
	if confirm "Install unofficial picamera supporting lens-shading (recommended)?"; then
		sudo $PIP_BIN install https://github.com/rwb27/picamera/releases/download/v1.13.1b0/picamera-1.13.1b0-py3-none-any.whl
	fi

	# Fix config directory ownership
	chown -R $user:$user "$userconfdir"

	# Handle server service
	newline
	echo -e "Installing microscope service"
	logfile="$userconfdir/openflexure_microscope.log"
	sudo cat > /etc/systemd/system/ofmserver.service <<- EOL
	[Unit]
	Description=Gunicorn instance to serve the OpenFlexure Microscope API
	After=network.target

	[Service]
	User=$user
	Group=www-data
	WorkingDirectory=$userhome
	Environment="PATH=$virtualenvdir/bin/"
	ExecStart=$virtualenvdir/bin/gunicorn --log-file $logfile --log-level info --threads 5 --workers 1 --graceful-timeout 3 --bind 0.0.0.0:5000 openflexure_microscope.api.app:app

	[Install]
	WantedBy=multi-user.target
	EOL

	if confirm "Start the server on boot (recommended)?"; then
		sudo systemctl enable ofmserver
	fi

	# Install clients
	
	EV_VERSION=0.2.0
	
	newline
	if confirm "Install the local OpenFlexure eV client (recommended)?"; then
		UPDATE_URL="https://openflexure.bath.ac.uk/build/openflexure_ev/${EV_VERSION}/openflexure-ev_${EV_VERSION}_armhf.deb"
		wget ${UPDATE_URL} -O /tmp/openflexure-ev_armhf.deb
		sudo apt-get install /tmp/openflexure-ev_armhf.deb --yes
	fi

	if confirm "Serve a lightweight version OpenFlexure eV client (recommended)?"; then
		sudo apt-get install lighttpd --yes
        
		UPDATE_URL="https://openflexure.bath.ac.uk/build/openflexure_ev/${EV_VERSION}/openflexure-ev_${EV_VERSION}_web.tar.gz"
		wget ${UPDATE_URL} -O /tmp/openflexure-ev_web.tar.gz
		mkdir -p "$userconfdir/ev_dist/www"
		tar -xvzf /tmp/openflexure-ev_web.tar.gz -C "$userconfdir/ev_dist/www"
		
        # Default ports
        PRIMARY_PORT=8000
        EV_PORT=80
		
		sudo cat > /etc/lighttpd/lighttpd.conf <<- EOL
		server.modules = (
			"mod_access",
			"mod_alias",
			"mod_compress",
			"mod_redirect",
		)

		server.document-root        = "/var/www/html"
		server.upload-dirs          = ( "/var/cache/lighttpd/uploads" )
		server.errorlog             = "/var/log/lighttpd/error.log"
		server.pid-file             = "/var/run/lighttpd.pid"
		server.username             = "www-data"
		server.groupname            = "www-data"
		server.port                 = $PRIMARY_PORT

		index-file.names            = ( "index.php", "index.html", "index.lighttpd.html" )
		url.access-deny             = ( "~", ".inc" )
		static-file.exclude-extensions = ( ".php", ".pl", ".fcgi" )

		compress.cache-dir          = "/var/cache/lighttpd/compress/"
		compress.filetype           = ( "application/javascript", "text/css", "text/html", "text/plain" )

		# default listening port for IPv6 falls back to the IPv4 port
		include_shell "/usr/share/lighttpd/use-ipv6.pl " + server.port
		include_shell "/usr/share/lighttpd/create-mime.assign.pl"
		include_shell "/usr/share/lighttpd/include-conf-enabled.pl"

		\$SERVER["socket"] == ":$EV_PORT" {
			server.document-root = "/home/pi/.openflexure/ev_dist/www"
		}
		EOL
		sudo systemctl restart lighttpd.service
	fi

# Environment finalisation and follow-up installers

	# Exit virtualenv
	deactivate
	echo -e "\nFinished installing OpenFlexure Microscope Server!"

	echo -e "\nInstalling RaspAP WiFi Configuration Portal allows your Raspberry Pi to broadcast its own wireless network, useful for remote control."
	# Install RaspAP
	if confirm "Install RaspAP WiFi Configuration Portal (Recommended)?"; then
		wget $raspapurl -O /tmp/raspap
		if [ "$FORCE" = true ]; then
			bash /tmp/raspap -y
		else
			bash /tmp/raspap
		fi
	else
		if confirm "The system needs to be rebooted as a final step. Reboot now?"; then
			sudo shutdown -r now || install_error "Unable to execute shutdown"
		fi
	fi

fi
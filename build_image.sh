#!/usr/bin/env bash

set -e

raspbian_version=2019-04-08-raspbian-stretch
zip="${raspbian_version}.zip"
raspbian_url="https://downloads.raspberrypi.org/raspbian/images/raspbian-2019-04-09/${zip}"
short_openflexure_version=$(git describe --always --dirty)
img="${raspbian_version}.img"
output_version="raspbian-openflexure-${short_openflexure_version}"
output_img="${output_version}.img" 
root_start_sector=98304 #check this is correct when changing raspbian version

if [ ! -f "${img}" ]; then
	if [ ! -f "${zip}" ]; then
		echo "downloading '${zip}'"
		wget "${raspbian_url}"	
	else
		echo "'${zip}' already downloaded, skipping"
	fi
	echo "extracting '${zip}'"
	unzip "${zip}"
else
	echo "'${img}' already exists, skipping download and extract"
fi

if [ ! -f "${output_img}" ]; then
	echo "copying to '${output_img}' and resizing"
	cp "${img}" "${output_img}"
	qemu-img resize "${output_img}" +'0.6G'
	(
	echo  d # delete partition
	echo  2 # partion number 2
	echo  n # new partition
	echo  p # primary partition
	echo  2 # partion number 2
	echo  "${root_start_sector}" # start sector, check this is still correct when changing raspbian version
	echo    # end sector, using default option which will fill to image size
	echo  w # save
	echo  q # and we're done
	) | fdisk "${output_img}"
else
	echo "'${output_img}' already exists, skipping copy and resize"
fi

expect install_on_image_using_qemu.expect "${output_img}"

if [ "$1" == "--upload-to-digitalocean" ]; then
    branch_zip="raspbian-openflexure-${CI_COMMIT_REF_NAME}.zip"
    zip "${output_version}.zip" "${output_img}"
    s3cmd -c .s3cfg put --acl-public "${output_version}.zip" 's3://openflexure-images'
    s3cmd -c .s3cfg put --acl-public "${output_version}.zip" "s3://openflexure-images/${branch_zip}"
fi

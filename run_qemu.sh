#!/bin/bash
dtb="versatile-pb.dtb"
kernel="kernel-qemu-4.14.79-stretch"

if [ ! -f "${dtb}" ]; then
    wget "https://openflexure-images.ams3.digitaloceanspaces.com/${dtb}"
fi

if [ ! -f "${kernel}" ]; then
    wget "https://openflexure-images.ams3.digitaloceanspaces.com/${kernel}"
fi

qemu-system-arm -kernel "${kernel}" \
    -cpu arm1176 -m 256 -M versatilepb \
    -dtb "${dtb}" -no-reboot \
    -serial 'mon:stdio' -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
    -drive "file=$1,index=0,media=disk,format=raw" \
    -net nic \
    -net user \
    -nographic \
    -virtfs "local,path=$(pwd),mount_tag=host0,security_model=passthrough,id=host0" \

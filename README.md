# openflexure-microscope-installer

## Raspbian Images

You can [download the latest pre-built image with the OpenFlexure software installed here](https://openflexure-images.ams3.digitaloceanspaces.com/raspbian-openflexure-master.zip).
If you want to download an older image you should be able to find it as

`https://openflexure-images.ams3.digitaloceanspaces.com/raspbian-openflexure-$ref.zip`

Where `$ref` is a git tag or 7-digit shortend commit sha from this repository (i.e. what you get when you run `git describe --always`).

If you want to build an image yourself you can use the `build_image.sh` script:

```
sudo apt update
sudo apt install qemu-system-arm unzip wget expect git
git clone https://gitlab.com/openflexure/openflexure-microscope-installer
cd openflexure-microscope-installer
./build_image.sh
```

This will download a Raspbian image, resize it and run the openflexure_microscope.sh script on that image inside qemu. It will output an image with a name like `raspbian-openflexure-*******.img`.

If you want to run an image in qemu yourself you can do:

```
./run_qemu.sh yourimage.img
```

## Usage
It's best to start with an SD card that has been flashed with the latest version of Raspbian.  
If your Raspbian installation is too old, it will probably fail; the most important thing is 
that you need to have piwheels set up (otherwise it tries to compile numpy which is a bad
idea).

`curl -sS https://gitlab.com/openflexure/openflexure-microscope-installer/raw/master/openflexure_microscope.sh |sudo bash`

or use the shorter forwarding URL:

`curl -LSs get.openflexure.org/microscope |sudo bash`

### Developer installation
`curl -LSs get.openflexure.org/microscope |sudo bash -s -- -d`

### Non-interactive installation
`curl -LSs get.openflexure.org/microscope |sudo bash -s -- -y`


## Warnings
**It is generally a horrible idea to curl a bash script and run it with sudo.** 

However, for the sake of convenience, we offer the option to use this all-in-one installation script.
You are downloading the script from *this* repository, and are encouraged to check that the script
is in no way malicious before proceeding. If using the shortened URL, the `-L` argument allows
`curl` to follow forwarding, which redirects to the full URL. This again should be checked
before running.

## What this does
* Checks your software and hardware environment for compatibility
* Installs all apt dependencies required to run the microscope server
    * **Required for numpy:** libatlas-base-dev, libjasper-dev, libjpeg-dev 
* Creates a `.openflexure` directory in your home folder
* Creates a virtual environment to isolate the server
* Installs packages required for GPIO functionality
* Enables the picamera
* Installs the openflexure-microscope Python distribution
    * **Developer mode:** Installs [Poetry](https://github.com/sdispater/poetry), clones the [openflexure-microscope-software repository](https://gitlab.com/openflexure/openflexure-microscope-server) into your home folder, and calls `poetry install`.
    * **User mode:** Installs the latest release of [openflexure-microscope](https://pypi.org/project/openflexure-microscope/) from PyPI.
* Installs the [Gunicorn](https://gunicorn.org) WSGI HTTP Server
* Creates a system.d service to start the microscope server with Gunicorn
    * (Optional) Enable the service to start on boot
* (Optional) Install the [OpenFlexure eV client](https://gitlab.com/openflexure/openflexure-microscope-jsclient)
* (Optional) Install [RaspAP](https://github.com/billz/raspap-webgui), for managing WiFi hotspot functionality
